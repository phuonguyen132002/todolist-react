import "./App.css";
import ToDoList from "./todolist/index.js";
// import HomeRedux from "./UserManagement-redux-review/index";
function App() {
  return (
    <div>
      {/* <HomeRedux /> */}
      <ToDoList />
    </div>
  );
}

export default App;
