const initialState = {
  taskList: [
    {
      id: 1,
      txtTask: "Nấu cơm",
      status: "incomplete",
    },
    {
      id: 2,
      txtTask: "Rửa chén",
      status: "incomplete",
    },
    {
      id: 3,
      txtTask: "Học bài",
      status: "completed",
    },
    {
      id: 4,
      txtTask: "Cho mèo ăn",
      status: "completed",
    },
  ],
};

const ToDoListReducer = (state = initialState, action) => {
  switch (action.type) {
    case "DELETE_TASK": {
      let taskList = [...state.taskList];
      let index = state.taskList.findIndex((task) => {
        return task.id === action.payload.id;
      });
      taskList.splice(index, 1);
      state.taskList = taskList;
      return { ...state };
    }
    //i lov u
    case "ADD_TASK": {
      let newTask = { ...action.payload, id: Math.random() };
      let taskList = [...state.taskList, newTask];
      state.taskList = taskList;
      return { ...state };
    }

    case "EDIT_TASK": {
      let taskList = [...state.taskList];
      let index = state.taskList.findIndex((task) => {
        return task.id === action.payload.id;
      });
      if (action.payload.status === "completed") {
        taskList[index] = { ...action.payload, status: "incomplete" };
      } else {
        taskList[index] = { ...action.payload, status: "completed" };
      }
      state.taskList = taskList;
      return { ...state };
    }

    default:
      return { ...state };
  }
};
export default ToDoListReducer;
