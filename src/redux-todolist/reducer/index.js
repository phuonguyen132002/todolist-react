import { combineReducers } from "redux";
import ToDoListReducer from "./todolist";

const rootReducer = combineReducers({
  ToDoListReducer,
});
export default rootReducer;
