import React, { Component } from "react";
import { connect } from "react-redux";

class AddUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      txtTask: "",
      status: "incomplete",
    };
  }

  handleOnChange = (event) => {
    const { value } = event.target;
    this.setState({
      txtTask: value,
    });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    this.props.addTask(this.state);
    this.setState({
      txtTask: "",
    });
  };
  render() {
    return (
      <form onSubmit={this.handleSubmit} className="card__add">
        <input
          id="newTask"
          type="text"
          placeholder="Enter an activity..."
          onChange={this.handleOnChange}
          value={this.state.txtTask}
        />
        <button id="addItem">
          <i className="fa fa-plus" />
        </button>
      </form>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addTask: (task) => {
      const action = {
        type: "ADD_TASK",
        payload: task,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(AddUser);
