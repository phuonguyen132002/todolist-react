import React, { Component } from "react";
import CompletedTask from "./completedTask";
import IncompleteTask from "./incompleteTask";
import { connect } from "react-redux";

class TaskList extends Component {
  renderIncompleteTable = () => {
    const { taskList } = this.props;
    return taskList.map((task) => {
      if (task.status === "incomplete") {
        return <IncompleteTask task={task} key={task.id} />;
      }
    });
  };
  renderCompletedTable = () => {
    const { taskList } = this.props;
    return taskList.map((task) => {
      if (task.status === "completed")
        return <CompletedTask task={task} key={task.id} />;
    });
  };
  render() {
    return (
      <div>
        <ul className="todo" id="todo">
          {this.renderIncompleteTable()}
        </ul>
        <ul className="todo" id="completed">
          {this.renderCompletedTable()}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    taskList: state.ToDoListReducer.taskList,
  };
};
export default connect(mapStateToProps, null)(TaskList);
