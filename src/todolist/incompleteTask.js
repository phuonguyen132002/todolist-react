import React, { Component } from "react";
import { connect } from "react-redux";

class IncompleteTask extends Component {
  render() {
    const { task } = this.props;
    return (
      <div>
        <li>
          <span>{task.txtTask}</span>
          <div className="buttons">
            <button
              className="remove"
              onClick={() => {
                this.props.deleteTask(task);
              }}
            >
              <i className="fa fa-trash-alt" />
            </button>
            <button
              className="complete"
              onClick={() => {
                this.props.editTask(task);
              }}
            >
              <i className="far fa-check-circle" />
              <i className="fas fa-check-circle" />
            </button>
          </div>
        </li>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteTask: (task) => {
      const action = {
        type: "DELETE_TASK",
        payload: task,
      };
      dispatch(action);
    },
    editTask: (task) => {
      const action = {
        type: "EDIT_TASK",
        payload: task,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(IncompleteTask);
